# SourceCode

ASCII-based library for generating and querying source code.

## Catalog

| Opam Package Version   | Max Catalog Date |
| ---------------------- | ---------------- |
| `SourceCode_Std.0.1.0` | `(2024,1,6)`     |
