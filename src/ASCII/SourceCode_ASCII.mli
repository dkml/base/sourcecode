(** ASCII-based library for generating and querying source code
    of different programming languages and different domain
    specific languages. *)

module Casing = Casing
