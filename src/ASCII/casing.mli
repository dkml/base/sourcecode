(** Convert Pascal case (ex. ["JohnDoe"]) to and from other cases like Camel case (ex. ["johnDoe"]) or Snake case (ex. ["john_doe"]) or Kebab case (ex. ["john-doe"]).

    {1 Terminology}
    - Pascal case: Words are separated on any transition from lower case character to a non-lower case character,
      and each word starts with a capital letter.
    - Camel case: Same as Pascal case except the first word is completely lowercase.
    - Snake case: Same as Pascal case except each word is completely lowercase, and words are separated by underscores (['_']).
    - Kebab case: Same as Snake case except words are separated by dashes (['-']).

    {1 Functions}
*)

val pascal_to_snake_case : string -> string

val pascal_to_camel_case : string -> string

val pascal_to_kebab_case : string -> string

val camel_to_snake_case : string -> string

val camel_to_pascal_case : string -> string

val camel_to_kebab_case : string -> string

val snake_to_camel_case : string -> string

(**/**)

module ForTesting : sig
  val trim_trailing_underscores : string -> string

  val decompose_pascal_or_camel : string -> string list

  val decompose_snake : string -> string list
end
