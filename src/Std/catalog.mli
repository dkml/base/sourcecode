(** Catalogs of programming languages and DSLs.
    
   Catalogs are useful when you need to accept user-created
   identifers yet check whether those identifiers are reserved
   by programming languages and DSLs*)

type t
(** The type of programming language and DSL catalog *)

type date = int * int * int
(** The type for big-endian proleptic Gregorian dates compatible with
    the popular OCaml package {{:https://v3.ocaml.org/p/ptime/latest}ptime}.
    
    A triple [(y, m, d)] with:
    {ul
    {- [y] the year from [0] to [9999]. [0] denotes -1 BCE
       (this follows the
       {{:http://www.iso.org/iso/home/standards/iso8601.htm}ISO 8601}
       convention).}
    {- [m] is the month from [1] to [12]}
    {- [d] is the day from [1] to [28], [29], [30] or [31]
       depending on [m] and [y]}}

    A date is said to be {e valid} iff the values [(y, m, d)] are
    in the range mentioned above and represent an existing date in the
    proleptic Gregorian calendar. *)

val create : date -> t option
(** [create catalog_date] creates a catalog that is a snapshot as of the date [catalog_date].

    If the [catalog_date] is {b past} the last catalogued date embedded in the current [SourceCode_Std]
    package, then [None] is returned.

    {b How to pick the Catalog Date?}

    The {{:https://gitlab.com/dkml/base/sourcecode#catalog}SourceCode README} has a table of
    dates you should pick based on the opam package version. Let's take for example the opam package
    ["SourceCode_Std.0.1.0"]. The table shows ["(2024,1,6)"] as the [Max Catalog Date]. You would:

    + Add [(SourceCode_Std (>= 0.1.0))] to your ["dune-project"] file.
    + Use [let catalog = SourceCode_Std.Catalog.create (2024,1,6)] in your source code.

    It is always safe to use a Catalog Date {i before} what is listed on the table.
*)

val tiobe_two_percenter : t -> Language.t list
(** [tiobe_two_percenter catalog] is a list of languages which have met or exceeded
    [2.0]% at any time in the date range between [(2023,1,1)] and the [catalog_date] of {!create}.

    The returned languages have a stable order but there is no intrinsic meaning to the
    order. That is, treat the languages like a set.

    Confer: {{:https://www.tiobe.com/tiobe-index/}TIOBE Index Ratings} *)

(**/**)

module ForExtensions : sig
  val catalog_date : t -> date
end
