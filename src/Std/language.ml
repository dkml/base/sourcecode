type t = Language of { name : string; reserved_keywords : string list }

let create ~name ~reserved_keywords () = Language { name; reserved_keywords }
let name (Language { name; _ }) = name
let reserved_keywords (Language { reserved_keywords; _ }) = reserved_keywords
