(** Standard library for generating and querying source code
    of different programming languages and different domain
    specific languages. *)

module Catalog = Catalog
module Language = Language

(** The known programming languages and DSLs. *)
module Languages : sig
  module C = C
  module CPP = Cpp
  module CSharp = Csharp
  module Java = Java
  module JavaScript = Javascript
  module OCaml = Ocaml
  module PHP = Php
  module Python = Python
  module SQL = Sql
  module VisualBasic = Visualbasic
end
