(** The implementation of a language. *)
module type S = sig
  type t
  (** The type of language. *)

  val language : t -> Language.t
  (** [language l] is the {!Language} for the instance [l]. *)
end
