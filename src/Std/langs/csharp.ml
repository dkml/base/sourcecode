type t = CSharp

(** {{:https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/}} *)
let reserved_keywords =
  [
    "abstract";
    "as";
    "base";
    "bool";
    "break";
    "byte";
    "case";
    "catch";
    "char";
    "checked";
    "class";
    "const";
    "continue";
    "decimal";
    "default";
    "delegate";
    "do";
    "double";
    "else";
    "enum";
    "event";
    "explicit";
    "extern";
    "false";
    "finally";
    "fixed";
    "float";
    "for";
    "foreach";
    "goto";
    "if";
    "implicit";
    "in";
    "int";
    "interface";
    "internal";
    "is";
    "lock";
    "long";
    "namespace";
    "new";
    "null";
    "object";
    "operator";
    "out";
    "override";
    "params";
    "private";
    "protected";
    "public";
    "readonly";
    "ref";
    "return";
    "sbyte";
    "sealed";
    "short";
    "sizeof";
    "stackalloc";
    "static";
    "string";
    "struct";
    "switch";
    "this";
    "throw";
    "true";
    "try";
    "typeof";
    "uint";
    "ulong";
    "unchecked";
    "unsafe";
    "ushort";
    "using";
    "virtual";
    "void";
    "volatile";
    "while";
  ]

let singleton_language = Language.create ~name:"C#" ~reserved_keywords ()
let csharp () = CSharp

let language (csharp : t) =
  ignore csharp;
  singleton_language
