type t = JDK8

(** {{:https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html}} *)
let reserved_keywords =
  [
    "abstract";
    "assert";
    "boolean";
    "break";
    "byte";
    "case";
    "catch";
    "char";
    "class";
    "const";
    "continue";
    "default";
    "do";
    "double";
    "else";
    "enum";
    "extends";
    "final";
    "finally";
    "float";
    "for";
    "goto";
    "if";
    "implements";
    "import";
    "instanceof";
    "int";
    "interface";
    "long";
    "native";
    "new";
    "package";
    "private";
    "protected";
    "public";
    "return";
    "short";
    "static";
    "strictfp";
    "super";
    "switch";
    "synchronized";
    "this";
    "throw";
    "throws";
    "transient";
    "try";
    "void";
    "volatile";
    "while";
  ]

let singleton_language = Language.create ~name:"Java" ~reserved_keywords ()
let jdk8 () = JDK8

let language (java : t) =
  ignore java;
  singleton_language
