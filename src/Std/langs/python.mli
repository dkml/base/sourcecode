include Lang.S

val python3 : unit -> t
(** [python3 ()] gets an instance of Python 3. *)
