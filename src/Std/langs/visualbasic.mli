include Lang.S

val visualbasic : unit -> t
(** [visualbasic ()] gets an instance of Visual Basic. *)
