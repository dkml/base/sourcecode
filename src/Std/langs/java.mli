include Lang.S

val jdk8 : unit -> t
(** [jdk8 ()] gets an instance of JDK 8. *)
