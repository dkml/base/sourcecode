type t = PHP8

(** {{:https://www.php.net/manual/en/reserved.keywords.php}} *)
let reserved_keywords =
  [
    "__halt_compiler";
    "abstract";
    "and";
    "array";
    "as";
    "break";
    "callable";
    "case";
    "catch";
    "class";
    "clone";
    "const";
    "continue";
    "declare";
    "default";
    "die";
    "do";
    "echo";
    "else";
    "elseif";
    "empty";
    "enddeclare";
    "endfor";
    "endforeach";
    "endif";
    "endswitch";
    "endwhile";
    "eval";
    "exit";
    "extends";
    "final";
    "finally";
    "fn";
    "for";
    "foreach";
    "function";
    "global";
    "goto";
    "if";
    "implements";
    "include";
    "include_once";
    "instanceof";
    "insteadof";
    "interface";
    "isset";
    "list";
    "match";
    "namespace";
    "new";
    "or";
    "PHP";
    "print";
    "private";
    "protected";
    "public";
    "readonly";
    "require";
    "require_once";
    "return";
    "static";
    "switch";
    "tests";
    "throw";
    "trait";
    "try";
    "unset";
    "use";
    "var";
    "while";
    "xor";
    "yield";
  ]

let singleton_language = Language.create ~name:"PHP" ~reserved_keywords ()
let php8 () = PHP8

let language (php : t) =
  ignore php;
  singleton_language
