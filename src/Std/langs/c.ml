type t = C23

(** {{:https://en.cppreference.com/w/c/keyword}} *)
let reserved_keywords =
  [
    "_Alignas";
    "_Alignof";
    "_Atomic";
    "_BitInt";
    "_Bool";
    "_Complex";
    "_Decimal128";
    "_Decimal32";
    "_Decimal64";
    "_Generic";
    "_Imaginary";
    "_Noreturn";
    "_Static_assert";
    "_Thread_local";
    "alignas";
    "alignof";
    "auto";
    "bool";
    "break";
    "case";
    "char";
    "const";
    "constexpr";
    "continue";
    "default";
    "do";
    "double";
    "else";
    "enum";
    "extern";
    "false";
    "float";
    "for";
    "goto";
    "if";
    "inline";
    "int";
    "long";
    "nullptr";
    "register";
    "restrict";
    "return";
    "short";
    "signed";
    "sizeof";
    "static";
    "static_assert";
    "struct";
    "switch";
    "thread_local";
    "true";
    "typedef";
    "typeof";
    "typeof_unqual";
    "union";
    "unsigned";
    "void";
    "volatile";
    "while";
  ]

let singleton_language = Language.create ~name:"C" ~reserved_keywords ()
let c23 () = C23

let language (c : t) =
  ignore c;
  singleton_language
