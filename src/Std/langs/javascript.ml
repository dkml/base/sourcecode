type t = Javascript

(** {{:https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#reserved_words}} *)
let reserved_keywords =
  [
    "break";
    "case";
    "catch";
    "class";
    "const";
    "continue";
    "debugger";
    "default";
    "delete";
    "do";
    "else";
    "export";
    "extends";
    "false";
    "finally";
    "for";
    "function";
    "if";
    "import";
    "in";
    "instanceof";
    "new";
    "null";
    "return";
    "super";
    "switch";
    "this";
    "throw";
    "true";
    "try";
    "typeof";
    "var";
    "void";
    "while";
    "with";
    (* The following are only reserved when they are found in strict mode code: *)
    "let";
    "static";
    "yield";
    (* The following are only reserved when they are found in module code or async function bodies: *)
    "await";
  ]

let singleton_language =
  Language.create ~name:"JavaScript" ~reserved_keywords ()

let javascript () = Javascript

let language (javascript : t) =
  ignore javascript;
  singleton_language
