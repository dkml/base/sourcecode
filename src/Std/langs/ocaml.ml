type t = OCaml5

(** {{:https://v2.ocaml.org/manual/lex.html#sss:keywords}} *)
let reserved_keywords =
  [
    "and";
    "as";
    "asr";
    "assert";
    "begin";
    "class";
    "constraint";
    "do";
    "done";
    "downto";
    "else";
    "end";
    "exception";
    "external";
    "false";
    "for";
    "fun";
    "function";
    "functor";
    "if";
    "in";
    "include";
    "inherit";
    "initializer";
    "land";
    "lazy";
    "let";
    "lor";
    "lsl";
    "lsr";
    "lxor";
    "match";
    "method";
    "mod";
    "module";
    "mutable";
    "new";
    "nonrec";
    "object";
    "of";
    "open";
    "or";
    "private";
    "rec";
    "sig";
    "struct";
    "then";
    "to";
    "true";
    "try";
    "type";
    "val";
    "virtual";
    "when";
    "while";
    "with";
  ]

let singleton_language = Language.create ~name:"OCaml" ~reserved_keywords ()
let ocaml5 () = OCaml5

let language (ocaml : t) =
  ignore ocaml;
  singleton_language
