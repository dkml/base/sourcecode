include Lang.S

val ocaml5 : unit -> t
(** [ocaml5 ()] gets an instance of OCaml 5. *)
