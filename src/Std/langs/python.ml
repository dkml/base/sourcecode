type t = Python3

(** {{:https://docs.python.org/3/reference/lexical_analysis.html#keywords}} *)
let reserved_keywords =
  [
    "False";
    "await";
    "else";
    "import";
    "pass";
    "None";
    "break";
    "except";
    "in";
    "raise";
    "True";
    "class";
    "finally";
    "is";
    "return";
    "and";
    "continue";
    "for";
    "lambda";
    "try";
    "as";
    "def";
    "from";
    "nonlocal";
    "while";
    "assert";
    "del";
    "global";
    "not";
    "with";
    "async";
    "elif";
    "if";
    "or";
    "yield";
  ]

let singleton_language = Language.create ~name:"Python" ~reserved_keywords ()

let python3 () = Python3

let language (python : t) =
  ignore python;
  singleton_language
