module Catalog = Catalog
module Language = Language

module Languages = struct
  module C = C
  module CPP = Cpp
  module CSharp = Csharp
  module Java = Java
  module JavaScript = Javascript
  module OCaml = Ocaml
  module PHP = Php
  module Python = Python
  module SQL = Sql
  module VisualBasic = Visualbasic
end
