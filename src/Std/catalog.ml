type lang = Lang : 'a * (module Lang.S with type t = 'a) -> lang
type date = int * int * int
type t = Snapshot of { dt : date; tiobe2p : lang list }

let language_compare l1 l2 =
  String.compare (Language.name l1) (Language.name l2)

let date_compare (y1, m1, d1) (y2, m2, d2) =
  match Int.compare y1 y2 with
  | 0 -> begin match Int.compare m1 m2 with 0 -> Int.compare d1 d2 | c -> c end
  | c -> c

module DateMap = Map.Make (struct
  type t = date

  let compare = date_compare
end)

let search_k_gte_x k ~x = date_compare k x >= 0

(* Catalog *)

let lang_c23 = Lang (C.c23 (), (module C))
let lang_cpp23 = Lang (Cpp.cpp23 (), (module Cpp))
let lang_csharp = Lang (Csharp.csharp (), (module Csharp))
let lang_jdk8 = Lang (Java.jdk8 (), (module Java))
let lang_javascript = Lang (Javascript.javascript (), (module Javascript))
let lang_php8 = Lang (Php.php8 (), (module Php))
let lang_python3 = Lang (Python.python3 (), (module Python))
let lang_sql2023 = Lang (Sql.sql2023 (), (module Sql))
let lang_visualbasic = Lang (Visualbasic.visualbasic (), (module Visualbasic))

let dt_2024_01_06 =
  Snapshot
    {
      dt = (2024, 1, 6);
      (* https://www.tiobe.com/tiobe-index/ *)
      tiobe2p =
        [
          lang_python3;
          lang_c23;
          lang_cpp23;
          lang_jdk8;
          lang_csharp;
          lang_javascript;
          lang_php8;
          lang_visualbasic;
          lang_sql2023;
        ];
    }

let snapshots =
  let open DateMap in
  (* ALWAYS ALWAYS ALWAYS when you are adding a date make it the DAY OF or
     an ANTICIPATED DAY SOON AFTER the opam publication. *)
  let ( & ) (Snapshot s as t) = add s.dt t in
  dt_2024_01_06 & empty

(* Accessors *)

let create catalog_date =
  (* Find first catalog with a date >= [catalog_date]. If there is none it is a failure. *)
  match DateMap.find_first_opt (search_k_gte_x ~x:catalog_date) snapshots with
  | Some (_date, catalog) -> Some catalog
  | None -> None

let tiobe_two_percenter (Snapshot { tiobe2p; _ }) =
  List.map
    (fun (Lang (lang, m)) ->
      let module M = (val m) in
      M.language lang)
    tiobe2p
  |> List.sort language_compare

module ForExtensions = struct
  let catalog_date (Snapshot { dt; _ }) = dt
end
