(** A programming language or domain specific language (DSL). *)

type t
(** The type of programming language or DSL *)

val create : name:string -> reserved_keywords:string list -> unit -> t
(** [create ~name ~reserved_keywords ()] creates a fresh language. *)

val name : t -> string
(** [name lang] is the name of the language. *)

val reserved_keywords : t -> string list
(** [reserved_keywords lang] are the reserved keywords in
    the language [lang]. *)
