open SourceCode_Std
open Tezt
open Tezt.Base

let error_msg = "expected %L, got %R"

(** [llisteq ~fut ~expect input] verifies the function under test
    [fut] applied to [input] results in the language name list [expected]. *)
let llisteq ?__LOC__ ~__FILE__
    ~(fut : Catalog.t -> SourceCode_Std.Language.t list) ~fn ~expect
    (dt : Catalog.date) =
  let yyyy, mm, dd = dt in
  let pp_sep fmt _v = Format.fprintf fmt "|" in
  Test.register ~__FILE__
    ~title:
      Format.(
        asprintf {|%s(%04d-%02d-%02d) is %a|} fn yyyy mm dd
          (pp_print_list ~pp_sep pp_print_string)
          expect)
    ~tags:[ fn ]
  @@ fun () ->
  let cat = Catalog.create dt in
  match cat with
  | None ->
      Test.fail ?__LOC__ "There was no catalog for date (%04d,%02d,%02d)" yyyy
        mm dd
  | Some cat ->
      let actual = List.map (fun l -> Language.name l) (fut cat) in
      Check.((expect = actual) ?__LOC__ (list string)) ~error_msg;
      unit

let () =
  let dt = (2024, 1, 6) in
  let fut = Catalog.tiobe_two_percenter in
  let fn = "tiobe_two_percenter" in
  llisteq ~__FILE__ ~__LOC__ ~fn ~fut
    ~expect:
      [
        "C";
        "C#";
        "C++";
        "Java";
        "JavaScript";
        "PHP";
        "Python";
        "SQL";
        "Visual Basic";
      ]
    dt

let () = Test.run ()
