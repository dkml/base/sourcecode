open SourceCode_ASCII.Casing
open Tezt
open Tezt.Base

let error_msg = "expected %L, got %R"

(** [string_equal ~fut ~expect input] verifies the function under test
    [fut] applied to [input] results in the string [expected]. *)
let streq ?__LOC__ ~__FILE__ ~fut ~fn ~expect input =
  Test.register ~__FILE__
    ~title:(Printf.sprintf {|%s("%s") is "%s"|} fn input expect)
    ~tags:[ fn ]
  @@ fun () ->
  Check.((expect = fut input) ?__LOC__ string) ~error_msg;
  unit

(** [slist_equal ~fut ~expect input] verifies the function under test
    [fut] applied to [input] results in the string list [expected]. *)
let slisteq ?__LOC__ ~__FILE__ ~fut ~fn ~expect input =
  let pp_sep fmt _v = Format.fprintf fmt "|" in
  Test.register ~__FILE__
    ~title:
      Format.(
        asprintf {|%s("%s") is %a|} fn input
          (pp_print_list ~pp_sep pp_print_string)
          expect)
    ~tags:[ fn ]
  @@ fun () ->
  Check.((expect = fut input) ?__LOC__ (list string)) ~error_msg;
  unit

let () =
  let fut = ForTesting.trim_trailing_underscores in
  let fn = "trim_trailing_underscores" in
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"" "";
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"abc" "abc";
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"abc" "abc___";
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"abc___d" "abc___d";
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"" "___"

let () =
  let fut = ForTesting.decompose_pascal_or_camel in
  let fn = "decompose_pascal_or_camel" in
  slisteq ~__FILE__ ~__LOC__ ~fn ~fut
    ~expect:[ "H"; "Captcha"; "Sitekey" ]
    "H_CaptchaSitekey";
  slisteq ~__FILE__ ~__LOC__ ~fn ~fut
    ~expect:[ "H"; "Captcha"; "Sitekey" ]
    "H_Captcha_Sitekey"

let () =
  let fut = ForTesting.decompose_snake in
  let fn = "decompose_snake" in
  slisteq ~__FILE__ ~__LOC__ ~fn ~fut
    ~expect:[ "h"; "captcha"; "sitekey" ]
    "h_captcha_sitekey";
  slisteq ~__FILE__ ~__LOC__ ~fn ~fut
    ~expect:[ "h"; "captcha"; "sitekey" ]
    "h_captcha___sitekey"

let () =
  let fut = camel_to_pascal_case in
  let fn = "camel_to_pascal_case" in
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"ContentInsetAdjustmentBehavior"
    "contentInsetAdjustmentBehavior"

let () =
  let fut = camel_to_snake_case in
  let fn = "camel_to_snake_case" in
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"content_inset_adjustment_behavior"
    "contentInsetAdjustmentBehavior"

let () =
  let fut = snake_to_camel_case in
  let fn = "snake_to_camel_case" in
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"contentInsetAdjustmentBehavior"
    "content_inset_adjustment_behavior"

let () =
  let fut = pascal_to_snake_case in
  let fn = "pascal_to_snake_case" in
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"hcaptcha_sitekey" "HCaptchaSitekey";
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"h_captcha_sitekey"
    "H_CaptchaSitekey";
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"h_captcha_sitekey"
    "H_Captcha_Sitekey";
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"h_captcha_sitekey"
    "H_____CaptchaSitekey"

let () =
  let fut = pascal_to_kebab_case in
  let fn = "pascal_to_kebab_case" in
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"h-captcha-sitekey"
    "H_Captcha_Sitekey"

let () =
  let fut = pascal_to_camel_case in
  let fn = "pascal_to_camel_case" in
  streq ~__FILE__ ~__LOC__ ~fn ~fut ~expect:"hcaptchaSitekey" "HCaptchaSitekey"

let () = Test.run ()
